import java.util.Scanner;

public class Lab4 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //task2();
        task3();
    }

    private static void task2() {
        System.out.println("Task 2 :Enter a number");
        double a = scanner.nextInt();
    /*if (a == 0)
        System.out.println("Impossible to divide");
    else
        System.out.println(100/a);*/
        double result = a != 0 ? (100 / a) : Double.MAX_VALUE;
        System.out.println(result);
    }

    private static void task3() {
        boolean result;
        System.out.println("Task 3 :Enter a ");
        int a = scanner.nextInt();
        System.out.println("Task 3 :Enter b ");
        int b = scanner.nextInt();
        System.out.println("Task 3 :Enter c ");
        int c = scanner.nextInt();
        System.out.println("Task 3 :Enter d ");
        int d = scanner.nextInt();
        result = (a % 2 == 0 && b % 4 == 0) || (c % 3 == 0 && d % 3 != 0);
        System.out.println("Result is " + result);
    }
}
