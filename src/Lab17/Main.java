package Lab17;

public class Main {
    public static void main(String[] args) {
        Passenger passenger = new Passenger();
        HeavyCar heavyCar = new HeavyCar(400, 200);
        heavyCar.loadBasket(201);
        System.out.println("Heavy Car basket volume " + heavyCar.basket.volume);
        heavyCar.loadBasket(100);
        System.out.println("Heavy Car basket volume " + heavyCar.basket.volume);
        heavyCar.unloadBasket();
        System.out.println("Heavy Car basket volume " + heavyCar.basket.volume);
        heavyCar.putPassenger(passenger);

        LightCar lightCar = new LightCar(200, 400, 4);
        lightCar.putPassenger(passenger);
        lightCar.outPassenger(passenger);

        Samosval samosval = new Samosval(600, 400);
        samosval.putBasketDown();
        samosval.putBasketUp();
        samosval.putPassenger(passenger);
    }
}
