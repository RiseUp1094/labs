package Lab17;

public class LightCar extends Car implements Passengerable {

    int places;

    public LightCar(int power, int mass, int places) {
        this.setPower(power);
        this.setMass(mass);
        this.places = places;
    }

    @Override
    public void putPassenger(Passenger passenger) {
        if (this.places == 0)
            System.out.println("Cant place more passengers");
        else
            this.places--;

    }

    @Override
    public void outPassenger(Passenger passenger) {
        if (this.places == 4)
            System.out.println("Car is empty");
    }
}
