package Lab17;

public class HeavyCar extends Car implements Passengerable {
    Basket basket = new Basket();

    public HeavyCar(int power, int basketMax) {
        setPower(power);
        this.basket.max = basketMax;
        this.basket.volume = 0;
    }

    private void setBasketVolume(int volume) {
        this.basket.volume = volume;
    }

    public void loadBasket(int volume) {
        if (volume > this.basket.max)
            System.out.println("Cant load this much");
        else
            setBasketVolume(volume);
    }

    public void unloadBasket() {
        setBasketVolume(0);
    }


    @Override
    public void putPassenger(Passenger passenger) {
        System.out.println("Cant put passenger in heavy car");
    }

    @Override
    public void outPassenger(Passenger passenger) {
        System.out.println("Cant put passenger in heavy car");
    }
}
