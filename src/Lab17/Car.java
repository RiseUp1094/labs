package Lab17;

public abstract class Car {
    private int mass;
    private int power;

    public int getMass() {
        return mass;
    }

    public int getPower() {
        return power;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public void setPower(int power) {
        this.power = power;
    }


}
