package Lab17;

public class Samosval extends HeavyCar {

    public Samosval(int power, int basketMax) {
        super(power, basketMax);
    }

    public void putBasketUp() {
        System.out.println("Basket is up");
    }

    public void putBasketDown() {
        System.out.println("Basket is down");
    }
}
