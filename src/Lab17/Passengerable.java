package Lab17;

public interface Passengerable {

    void putPassenger(Passenger passenger);

    void outPassenger(Passenger passenger);
}
