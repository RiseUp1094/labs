package Lab11;

public class Adress {
    String country;
    String city;
    String street;
    String home;
    String apartNumber;
    String zipcode;

    public Adress(String country, String city, String street, String home, String apartNumber, String zipcode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.home = home;
        this.apartNumber = apartNumber;
        this.zipcode = zipcode;
    }

    public Adress(String country, String city, String street, String home, String apartNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.home = home;
        this.apartNumber = apartNumber;
    }
}
