package Lab11;

public class Person {
    String firstName;
    String secondName;
    String middleName;
    final String birthday;
    Adress adress;
    Person spouse;
    Person kid1;
    Person kid2;

    public Person(String firstName, String secondName, String middleName, String birthday) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.birthday = birthday;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setSpouse(Person spouse) {
        this.spouse = spouse;
    }
}
