package Lab11;

public class Main {

    public static void main(String args[]) {
        Adress adress1 = new Adress("Russia", "Saint-P", "Korablestroitelei", "46/1", "547", "199397");
        Adress adress2 = new Adress("test2", "Saint-P", "asd", "sss", "321");
        System.out.println(adress1.country + " " + adress1.city + " " + adress1.street + " " + adress1.home + " " + adress1.apartNumber + " " + adress1.zipcode);
        System.out.println(adress2.country + " " + adress2.city + " " + adress2.street + " " + adress2.home + " " + adress2.apartNumber + " ");

        Person person = new Person("Artur", "Omelnichenko", "Romanovich", "29.12.1995");
        person.setAdress(adress1);
        person.setSpouse(new Person("Anastasia", "Omelnichenko", "Eduardovna", "04.03.1995"));
    }
}
