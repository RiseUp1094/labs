package Lab14;

public class Main {

    static final int constant = 7;

    public static float humanAge(BasicCat cat) {
        return cat.age * constant;
    }

    public static void main(String[] args) {
        BasicCat cat1 = new BasicCat("pusya", 2, 15);
        System.out.println("Cat " + cat1.name + " human age = " + humanAge(cat1));
        BasicCat cat2 = new BasicCat("dusya", 0.5f, 12);
        System.out.println("Cat " + cat2.name + " human age = " + humanAge(cat2));
        BasicCat cat3 = new BasicCat("musya", 1, 15);
        System.out.println("Cat " + cat3.name + " human age = " + humanAge(cat3));
        System.out.println("numKittens is " + BasicCat.numKittens);
        System.out.println("Number of cats is " + BasicCat.cats.size());
    }
}
