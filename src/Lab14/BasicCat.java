package Lab14;

import java.util.ArrayList;
import java.util.List;

public class BasicCat {
    String name;
    float age;
    float weight;
    static int numKittens;
    static List<BasicCat> cats = new ArrayList<BasicCat>();

    BasicCat(String name, float age, float weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        if (age < 1)
            numKittens++;
        cats.add(this);
    }
}
