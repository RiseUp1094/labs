import java.util.Scanner;

public class Lab5 {

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        task3();
    }

    public static void task1() {
        System.out.println("Enter number ");
        int number = scanner.nextInt();
        switch (number) {
            case 0:
                System.out.println("It's winter");
                break;
            case 1:
                System.out.println("It's spring");
                break;
            case 2:
                System.out.println("It's summer");
                break;
            case 3:
                System.out.println("It's autumn");
                break;
            default:
                System.out.println("No such number");
                break;
        }
    }

    public static void task2() {
        System.out.println("Enter a ");
        int a = scanner.nextInt();
        System.out.println("Enter b ");
        int b = scanner.nextInt();
        System.out.println("Enter c ");
        int c = scanner.nextInt();
        if (b > a && b < c) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }

    public static void task3() {
        System.out.println("Enter number ");
        int number = scanner.nextInt();
        int factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial = factorial * i;
        }
        System.out.println("Factorial is " + factorial);
    }
}
