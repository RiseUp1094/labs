import java.util.Scanner;

public class Lab7 {

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        task2();
    }

    private static void task2() {
        System.out.println("Enter a value 0-4");
        int userChoice = scanner.nextInt();
        int opponentValue = (int) Math.round(Math.random() * 4);
        System.out.println("You chose " + choice(userChoice));
        System.out.println("Computer chose " + choice(opponentValue));
        String result = computing(userChoice, opponentValue);
        System.out.println("Result is " + result);
    }

    private static String computing(int first, int second) {
        String result = null;
        int sub = Math.abs((first - second) % 5);
        if (sub == 0)
            result = "Draw";
        if (sub == 1 | sub == 3)
            result = "You win";
        if (sub == 2 | sub == 4)
            result = "You lose";
        return result;
    }

    private static String choice(int number) {
        String result = null;
        switch (number) {
            case 0:
                result = "Rock";
                break;
            case 1:
                result = "Paper";
                break;
            case 2:
                result = "Scissors";
                break;
            case 3:
                result = "Spock";
                break;
            case 4:
                result = "Lizard";
                break;
            default:
                System.out.println("Wrong value");
                break;
        }
        return result;
    }

    private static void task1() {
        System.out.println("Enter number ");
        int number = scanner.nextInt();
        for (int i = 0; i < number; i++) {
            System.out.println(fibonacciRecursive(i));
        }
        fibonacciIterative(number);
    }

    public static int fibonacciRecursive(int number) {
        if (number == 0)
            return 0;
        if (number == 1)
            return 1;
        return fibonacciRecursive(number - 1) + fibonacciRecursive(number - 2);
    }

    public static void fibonacciIterative(int number) {
        int[] array = new int[number];
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < number; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }
        for (int i = 0; i < number; i++) {
            System.out.println(array[i]);
        }
    }
}
