package Control.vehicles;

public class Car extends Vehicle {

    private boolean engineState;

    public Car(String model, int speed, int speedNow) {
        super(model, speed, speedNow);
        this.engineState = false;
    }

    public boolean getEngineState() {
        return this.engineState;
    }

    public boolean setEngineState(boolean state) {
        return this.engineState = state;
    }

    public void changeEngineState(boolean engineState) {
        if (getEngineState() == engineState)
            System.out.println("Cant change engine state to existing one");
        else
            setEngineState(engineState);
        System.out.println("Current engine state is " + getEngineState());
    }

    public void changeSpeedBy(int number) {
        if (getEngineState() == false) {
            System.out.println("Speed can’t be changed because the car engine is off");
            return;
        }
        int newSpeed = getSpeedNow() + number;
        if (newSpeed > getSpeedMax())
            System.out.println("Cant increase speed more than max");
        else
            setSpeedNow(newSpeed);
        System.out.println("Current speed is " + getSpeedNow());
    }

    public void stop() {
        setSpeedNow(0);
        System.out.println("Current speed is " + getSpeedNow());
    }
}
