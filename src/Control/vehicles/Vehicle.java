package Control.vehicles;

public abstract class Vehicle {

    String model;
    private final int speedMax;
    private int speedNow;

    public Vehicle(String model, int speedMax, int speedNow) {
        this.model = model;
        this.speedMax = speedMax;
        this.speedNow = speedNow;
    }

    public int getSpeedMax() {
        return this.speedMax;
    }

    public int getSpeedNow() {
        return this.speedNow;
    }

    public void setSpeedNow(int speedNow) {
        this.speedNow = speedNow;
    }

    public void changeSpeedBy(int number) {
        int newSpeed = getSpeedNow() + number;
        if (newSpeed > getSpeedMax())
            System.out.println("Cant increase speed more than max");
        else
            setSpeedNow(newSpeed);
        System.out.println("Current speed is " + getSpeedNow());
    }

    public void stop() {
        setSpeedNow(0);
        System.out.println("Current speed is " + getSpeedNow());
    }
}
