package Control;

import Control.vehicles.Bicycle;
import Control.vehicles.Car;

public class Traffic {

    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle("Velik", 20, 0);
        Car car = new Car("Mashina", 200, 0);

        bicycle.changeSpeedBy(10); //current speed is 10

        bicycle.changeSpeedBy(1000); //current speed is 10

        bicycle.stop(); //current speed is 0, you stopped the vehicle

        car.changeSpeedBy(100); // ‘Speed can’t be changed because the car engine is off

        car.changeEngineState(true); // the engine is on

        car.changeSpeedBy(100); //current speed is 100

        car.stop(); //current speed is 0, you stopped the vehicle
    }
}
