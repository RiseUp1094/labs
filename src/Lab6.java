public class Lab6 {

    public static void main(String[] args) {
        task1();
        task2();
        task3();
    }

    private static void task3() {
        int count = 1;
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6, 9},
                {7}
        };
        int length = array.length;
        for (int i = 0; i < length; i++) {
            System.out.println("Row " + i + " length is " + array[i].length);
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (count % 2 == 0) {
                    System.out.println(array[i][j]);
                }
                count++;
            }
        }
    }

    private static void task2() {
        int sum = 0;
        int[] myList = {1, -2, 5, 3, -3, -4};
        for (int element : myList) {
            System.out.println(element);
        }
        for (int i = 0; i < myList.length; i++) {
            if (myList[i] > 0 && i % 2 == 0) {
                sum = sum + myList[i];
            }
        }
        System.out.println("Sum is " + sum);
    }

    private static void task1() {
        double[] myList = {1.9, 2.9, 3.4, 3.5};
        int n = myList.length - 1;
        double value;
        for (double element : myList) {
            System.out.println(element);
        }
        for (int i = 0; i < myList.length / 2; i++) {
            value = myList[n];
            myList[n] = myList[i];
            myList[i] = value;
            n--;
        }
        System.out.println("reversed list:");
        for (double element : myList) {
            System.out.println(element);
        }
    }
}
