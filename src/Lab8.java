import java.util.Scanner;

public class Lab8 {

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
    }

    private static void task4() {
        System.out.println("Enter number ");
        Integer value = scanner.nextInt();
        String string = value.toString();
        System.out.println("String is " + string);
        boolean isNotEven = string.length() % 2 != 0;
        System.out.println("Number of symbols is not even: " + isNotEven);
    }

    private static void task3() {
        int count = 0;
        System.out.println("Enter string ");
        String string = scanner.next();
        System.out.println("Enter char to count");
        char value = scanner.next().charAt(0);
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == value)
                count++;
        }
        System.out.println("Number of " + value + " in " + string + " is " + count);
    }

    private static boolean task2() {
        System.out.println("Enter string ");
        String string = scanner.next();
        boolean result = string.endsWith("ood");
        System.out.println("Result is " + result);
        return result;
    }

    private static void task1() {
        System.out.println("Enter your name: ");
        String name = scanner.next();
        System.out.println("Hello " + name);
    }
}
