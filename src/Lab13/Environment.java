package Lab13;

import stub.Simulator;
import test.TestCase;
import test.TestChain;

public class Environment {
    public static void main(String[] args) {
        System.out.println(TestCase.startTestCase() + TestChain.startTestChain() + Simulator.startSimulator());
    }
}
