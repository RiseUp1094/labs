package Lab10.ovens;

public abstract class Oven {
    String type;
    double temp;
    boolean isTurnedOn;

    public Oven(String type, double temp, boolean isTurnedOn) {
        this.type = type;
        this.temp = temp;
        this.isTurnedOn = isTurnedOn;
    }
}
