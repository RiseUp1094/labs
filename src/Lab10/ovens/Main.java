package Lab10.ovens;

public class Main {

    public static void main(String args[]) {
        GasOven gasOven = new GasOven("Gas", 0, false);
        ElectricOven electricOven = new ElectricOven("Electric", 0, false);
        Cooker cooker = new Cooker();
        cooker.turnOnOven(gasOven);
        cooker.turnOnOven(electricOven);
        cooker.changeTemp(gasOven, 5);
        cooker.changeTemp(electricOven, 20);
    }
}
