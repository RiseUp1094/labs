package Lab10.figures;

import java.awt.*;

public class Main {
    public static void main(String args[]) {
        Rectangle rectangle = new Rectangle(3, 4, new Point(3, 4));
        double p = rectangle.perimeter();
        double s = rectangle.area();
        System.out.println("Rectangle perimeter with height " + rectangle.height + " and width " + rectangle.width + " is " + p);
        System.out.println("Rectangle area with height " + rectangle.height + " and width " + rectangle.width + " is " + s);

        Rectangle square = new Rectangle(3, 3, new Point(3, 4));
        p = square.perimeter();
        s = square.area();
        System.out.println("Square perimeter with height " + square.height + " and width " + square.width + " is " + p);
        System.out.println("Square area with height " + square.height + " and width " + square.width + " is " + s);

        Circle circle = new Circle(4, new Point(3, 4));
        p = square.perimeter();
        s = square.area();
        System.out.println("Circle perimeter with radius " + circle.radius + " is " + p);
        System.out.println("Circle perimeter with radius " + circle.radius + " is " + s);
    }
}
