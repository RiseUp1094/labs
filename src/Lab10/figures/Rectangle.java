package Lab10.figures;

import java.awt.*;

public class Rectangle extends Figure {

    public Rectangle(int height, int width, Point point) {
        super(height, width, point);
    }

    @Override
    public double area() {
        return this.width * this.height;
    }

    @Override
    public double perimeter() {
        return (this.width + this.height) * 2;
    }
}
