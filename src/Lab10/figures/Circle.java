package Lab10.figures;

import java.awt.*;

public class Circle extends Figure {

    public Circle(int radius, Point point) {
        super(radius, point);
    }

    @Override
    public double area() {
        return (double) (3.14 * this.radius * this.radius);
    }

    @Override
    public double perimeter() {
        return (double) (this.radius * 2 * 3.14);
    }
}
