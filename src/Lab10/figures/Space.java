package Lab10.figures;

public interface Space {

    public double area();

    public double perimeter();

}
