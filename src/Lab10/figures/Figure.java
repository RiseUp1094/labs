package Lab10.figures;

import java.awt.*;

public abstract class Figure implements Space {
    int height;
    int width;
    double radius;
    Point point;

    public Figure(int height, int width, Point point) {
        this.height = height;
        this.width = width;
        this.point = point;
    }

    public Figure(int radius, Point point) {
        this.radius = radius;
        this.point = point;
    }
}

