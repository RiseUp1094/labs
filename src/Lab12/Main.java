package Lab12;

public class Main {
    public static void main(String[] args) {
        Adress adress1 = new Adress("Russia", "Saint-P", "Korablestroitelei", "46/1", "547", "199397");
        Adress adress2 = new Adress("Russia");
        Person person1 = new Person("Artur", 25, adress1, adress1);
        Person person2 = new Person("Nastya", 25, adress1, adress1);
        Person person3 = new Person("Random", 34, adress2, adress2);
        person1.showWorkAdress();
        person2.showWorkAdress();
        person3.showWorkAdress();
    }
}
