package Lab12;

public class Adress {
    String country;
    String city;
    String street;
    String home;
    String apartNumber;
    String zipcode;

    public Adress() {
    }

    public Adress(String country) {
        this.country = country;
    }

    public Adress(String country, String city, String street, String home, String apartNumber, String zipcode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.home = home;
        this.apartNumber = apartNumber;
        this.zipcode = zipcode;
    }

    public Adress(String country, String city, String street, String home, String apartNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.home = home;
        this.apartNumber = apartNumber;
    }

    public String getAdress() {
        String tmp = this.country + " " + this.city + " " +
                this.street + " " + this.home + " " +
                this.apartNumber + " " + this.zipcode;
        return tmp.replace("null", "");
    }
}
