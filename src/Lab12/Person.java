package Lab12;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Person {
    String name;
    int age;
    private Adress homeAdress;
    private Adress workAdress;

    public Person(String name, int age, Adress homeAdress, Adress workAdress) {
        this.age = age;
        this.name = name;
        this.homeAdress = homeAdress;
        this.workAdress = workAdress;
    }

    private void showHomeAdress() {
        List<Field> fieldlist = new ArrayList<Field>();
        while (this.homeAdress != null) {
            fieldlist.addAll(Arrays.asList(this.homeAdress.getClass().getDeclaredFields()));
        }
        for (Field field : fieldlist) {
            if (field != null)
                System.out.println(field.getName());
        }
    }

    public void showWorkAdress() {
        System.out.println(this.workAdress.getAdress());
    }
}
