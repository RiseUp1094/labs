package Lab15.ovens;

public abstract class Oven {
    String type;
    Double temp;
    boolean isTurnedOn;
    final static Integer min = 0;
    final static Integer max = 300;

    public Oven(String type, Double temp, boolean isTurnedOn) {
        this.type = type;
        this.temp = temp;
        this.isTurnedOn = isTurnedOn;
    }
}
