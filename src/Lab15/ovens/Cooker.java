package Lab15.ovens;

public class Cooker {

    public void turnOnOven(Oven oven) {
        if (oven.isTurnedOn) {
            System.out.println("Oven " + oven.type + " is already turned on");
        } else {
            oven.isTurnedOn = true;
            System.out.println("Oven " + oven.type + " is turned on " + oven.isTurnedOn);
        }
    }

    public void turnOffOven(Oven oven) {
        if (!oven.isTurnedOn) {
            System.out.println("Oven " + oven.type + "  is already turned off");
        } else {
            oven.isTurnedOn = false;
            System.out.println("Oven " + oven.type + " is turned on " + oven.isTurnedOn);
        }
    }

    public void changeTemp(Oven oven, Double temp) {
        Double oldTemp = oven.temp;
        oven.temp = oven.temp + temp;
        if (oven.temp > Oven.min && oven.temp < Oven.max && !oldTemp.equals(oven.temp))
            System.out.println("New temperature of " + oven.type + " is " + oven.temp);
        else
            System.out.println("New temperature is wrong");
    }
}