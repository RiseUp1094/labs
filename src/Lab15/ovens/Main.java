package Lab15.ovens;

public class Main {

    public static void main(String args[]) {
        GasOven gasOven = new GasOven("Gas", Double.valueOf(0), false);
        ElectricOven electricOven = new ElectricOven("Electric", Double.valueOf(0), false);
        Cooker cooker = new Cooker();
        cooker.turnOnOven(gasOven);
        cooker.turnOnOven(electricOven);
        cooker.changeTemp(gasOven, Double.valueOf(5));
        cooker.changeTemp(electricOven, Double.valueOf(20));
    }
}
