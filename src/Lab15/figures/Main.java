package Lab15.figures;

import java.awt.*;

public class Main {
    public static void main(String args[]) {
        Rectangle rectangle = new Rectangle(Double.valueOf(3), Double.valueOf(4), new Point(3, 4));
        Double p = Double.valueOf(rectangle.perimeter());
        Double s = Double.valueOf(rectangle.area());
        System.out.println("Rectangle perimeter with height " + rectangle.height + " and width " + rectangle.width + " is " + p);
        System.out.println("Rectangle area with height " + rectangle.height + " and width " + rectangle.width + " is " + s);
        Rectangle square = new Rectangle(Double.valueOf(3), Double.valueOf(3), new Point(3, 4));
        p = Double.valueOf(square.perimeter());
        s = Double.valueOf(square.area());
        System.out.println("Square perimeter with height " + square.height + " and width " + square.width + " is " + p);
        System.out.println("Square area with height " + square.height + " and width " + square.width + " is " + s);

        Circle circle = new Circle(Double.valueOf(4), new Point(3, 4));
        p = Double.valueOf(square.perimeter());
        s = Double.valueOf(square.area());
        System.out.println("Circle perimeter with radius " + circle.radius + " is " + p);
        System.out.println("Circle perimeter with radius " + circle.radius + " is " + s);
    }
}
