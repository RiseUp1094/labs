package Lab15.figures;

import java.awt.*;

public class Rectangle extends Figure {

    public Rectangle(Double height, Double width, Point point) {
        super(height, width, point);
    }

    @Override
    public Double area() {
        return this.width * this.height;
    }

    @Override
    public Double perimeter() {
        return (this.width + this.height) * 2;
    }
}
