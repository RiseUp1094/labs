package Lab15.figures;

import java.awt.*;

public abstract class Figure implements Space {
    Double height;
    Double width;
    Double radius;
    Point point;

    public Figure(Double height, Double width, Point point) {
        this.height = height;
        this.width = width;
        this.point = point;
    }

    public Figure(Double radius, Point point) {
        this.radius = radius;
        this.point = point;
    }
}

