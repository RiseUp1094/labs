package Lab15.figures;

public interface Space {

    public Double area();

    public Double perimeter();

}
