package Lab15.figures;

import java.awt.*;

public class Circle extends Figure {

    public Circle(Double radius, Point point) {
        super(radius, point);
    }

    @Override
    public Double area() {
        return (Double) (3.14 * this.radius * this.radius);
    }

    @Override
    public Double perimeter() {
        return (Double) (this.radius * 2 * 3.14);
    }
}
